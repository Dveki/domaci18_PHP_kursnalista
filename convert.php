<?php

include('db.php');

$iznos = $_POST['iznos'];
$valuta = $_POST['valuta'];
$kurs = $_POST['kurs'];

$sql = "SELECT " . $kurs . " FROM kurs_dinara WHERE valuta='". $valuta ."'";

$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

foreach($result AS $res){

  $vrednost = $res[$kurs];

}

$sql_valuta = "SELECT naziv FROM valuta WHERE id='". $valuta ."'";

$result_valuta = mysqli_query($connection,$sql_valuta) or die(mysqli_error($connection));

foreach($result_valuta AS $res_valuta){

  $valuta_naziv = $res_valuta['naziv'];

}
$konv_vrednost = $iznos*$vrednost;

//print $iznos . ' ' .$valuta_naziv . ' = ' .$konv_vrednost;

$date = new DateTime();
//echo date('d/m/Y H:i:s', $date->getTimestamp());
$date_echo = date('d/m/y', $date->getTimestamp());
$date_now = date('Y/m/d', $date->getTimestamp());
$time_now = date('H:i:s', $date->getTimestamp());


$sql_konverzija = "INSERT INTO konverzija(datum,vreme,iznos,valuta,kurs,iznos_kursa,konv_iznos)
        VALUES ('$date_now','$time_now','$iznos','$valuta','$kurs','$vrednost','$konv_vrednost')";

      $result = mysqli_query($connection,$sql_konverzija) or die(mysqli_error($connection));
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Online menjacnica konvertor</title>
</head>

<body>
    <div id="header"></div>
    <marquee>DJIA 25090.48 -84.83 -0.34% S&P 500 2779.66 -2.83 -0.10% NASDAQ 7746.38 -14.66 -0.19% CNBC IQ 100 277.9826 --- UNCH 0%
        RUSS 2K 1683.91 -0.82 -0.05% FTSE 7633.91 -131.88 -1.70% DAX 13010.55 -96.55 -0.74% NIKKEI 22851.75 113.14 0.50%
        HSI 30309.49 -130.68 -0.43%</marquee>
    <div id="ispis">
     
        <p>
            Konvertor valute:
            <br> Valuta države:
            <?php echo($valuta_naziv);?>
            <br> Iznos za konvertovanje u dinare:
            <?php echo(number_format((float)$iznos, 2, ',', ''));?>
            <br> Konvertovani iznos u dinarima:
            <?php echo(number_format((float)$konv_vrednost, 2, ',', ''));?>
            <br> Datum transfera:
            <?php echo($date_echo);?>
            <br> Vreme transfera:
            <?php echo($time_now);?>
        </p>

    </div>
    <a href="convlist.json" target="_blank">Lista svih konverzija</a>
    <br>
    <a href="index.php">Povratak na pocetnu stranu</a>
</body>

</html>     
            