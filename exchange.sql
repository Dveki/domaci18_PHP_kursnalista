-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 15, 2018 at 03:34 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exchange`
--

-- --------------------------------------------------------

--
-- Table structure for table `konverzija`
--

DROP TABLE IF EXISTS `konverzija`;
CREATE TABLE IF NOT EXISTS `konverzija` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `vreme` time NOT NULL,
  `iznos` float NOT NULL,
  `valuta` varchar(6) NOT NULL,
  `kurs` varchar(30) NOT NULL,
  `iznos_kursa` float NOT NULL,
  `konv_iznos` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `konverzija`
--

INSERT INTO `konverzija` (`id`, `datum`, `vreme`, `iznos`, `valuta`, `kurs`, `iznos_kursa`, `konv_iznos`) VALUES
(1, '2018-06-14', '18:38:24', 555, '5', 'prodajni', 15.9051, 8827.33),
(2, '2018-06-14', '18:39:04', 895, '1', 'srednji', 118.167, 105759),
(3, '2018-06-14', '18:39:14', 1500, '2', 'kupovni', 99.7648, 149647),
(4, '2018-06-14', '18:51:08', 568, '9', 'srednji', 12.4931, 7096.08),
(5, '2018-06-14', '18:51:33', 1000, '3', 'prodajni', 101.866, 101866),
(6, '2018-06-15', '09:34:19', 450, '5', 'srednji', 15.8575, 7135.88),
(7, '2018-06-15', '09:35:45', 895, '1', 'srednji', 118.193, 105783),
(8, '2018-06-15', '09:36:25', 1500, '2', 'kupovni', 101.795, 152692),
(9, '2018-06-15', '15:11:15', 569, '8', 'srednji', 76.2681, 43396.6);

-- --------------------------------------------------------

--
-- Table structure for table `kurs_dinara`
--

DROP TABLE IF EXISTS `kurs_dinara`;
CREATE TABLE IF NOT EXISTS `kurs_dinara` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `valuta` varchar(6) NOT NULL,
  `kupovni` float NOT NULL,
  `srednji` float NOT NULL,
  `prodajni` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kurs_dinara`
--

INSERT INTO `kurs_dinara` (`id`, `datum`, `valuta`, `kupovni`, `srednji`, `prodajni`) VALUES
(1, '2018-06-14', '1', 117.812, 118.167, 118.521),
(2, '2018-06-14', '2', 99.7648, 100.065, 100.365),
(3, '2018-06-14', '3', 101.257, 101.562, 101.866),
(4, '2018-06-14', '4', 133.619, 134.021, 134.424),
(5, '2018-06-14', '8', 75.4578, 75.6849, 75.912),
(6, '2018-06-14', '7', 76.906, 77.1374, 77.3688),
(7, '2018-06-14', '6', 11.5908, 11.6257, 11.6606),
(8, '2018-06-14', '5', 15.8099, 15.8575, 15.9051),
(9, '2018-06-14', '9', 12.4556, 12.4931, 12.5306),
(10, '2018-06-14', '0', 0.905691, 0.908416, 0.911141),
(11, '2018-06-14', '0', 1.593, 1.5978, 1.6026),
(12, '2018-06-14', '0', 15.5958, 15.6427, 15.6896),
(13, '2018-06-15', '1', 117.838, 118.193, 118.547),
(14, '2018-06-15', '2', 101.795, 102.101, 102.408),
(15, '2018-06-15', '3', 102.024, 102.331, 102.638),
(16, '2018-06-15', '4', 134.903, 135.309, 135.715),
(17, '2018-06-15', '8', 76.0393, 76.2681, 76.4969),
(18, '2018-06-15', '7', 77.5505, 77.7839, 78.0173),
(19, '2018-06-15', '6', 11.6408, 11.6758, 11.7108),
(20, '2018-06-15', '5', 15.8144, 15.862, 15.9096),
(21, '2018-06-15', '9', 12.5105, 12.5481, 12.5857),
(22, '2018-06-15', '0', 0.918242, 0.921005, 0.923768),
(23, '2018-06-15', '0', 1.6287, 1.6336, 1.6385),
(24, '2018-06-15', '0', 15.8679, 15.9156, 15.9633);

-- --------------------------------------------------------

--
-- Table structure for table `valuta`
--

DROP TABLE IF EXISTS `valuta`;
CREATE TABLE IF NOT EXISTS `valuta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(30) NOT NULL,
  `kod` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `valuta`
--

INSERT INTO `valuta` (`id`, `naziv`, `kod`) VALUES
(1, 'Euro', 'eur'),
(2, 'Americki Dolar', 'usd'),
(3, 'Svajcarski Franak', 'chf'),
(4, 'Britanska Funta', 'gbp'),
(5, 'Danska Kruna', 'dkk'),
(6, 'Svedska Kruna', 'sek'),
(7, 'Kanadski Dolar', 'cad'),
(8, 'Australijski dolar', 'aud'),
(9, 'Norveska Kruna', 'nok');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
