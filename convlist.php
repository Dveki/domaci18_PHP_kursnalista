<?php

  include('db.php');
 // used INNER JOIN to list insted of numbers for valuta, full name
  $sql = "SELECT konverzija.id, konverzija.datum, konverzija.vreme, konverzija.iznos, konverzija.kurs, konverzija.iznos_kursa, konverzija.konv_iznos, valuta.naziv FROM konverzija INNER JOIN valuta ON konverzija.valuta =valuta.id ORDER BY konverzija.datum ASC ";
  $result = mysqli_query($connection,$sql) or die(mysql_error());


  if ($result->num_rows > 0) {
    
    $json_response = array();
    while($row = $result->fetch_assoc()) {
        $datum = $row['datum'];
        $datum_temp = explode('-', $datum);
        $datum_new = ($datum_temp[2].'-'.$datum_temp[1].'-'.$datum_temp[0]);

        if (!isset($json_response[ $datum_new ])) {
            $json_response[ $datum_new ] = [
                //'datum' => $datum_new,
                'Konverzije' => [],
            ];
        }
        $json_response[ $datum_new ]['Konverzije'][] = [
                'vreme' => $row['vreme'],
                'iznos' => $row['iznos'],
                'valuta' => $row['naziv'],
                'kurs' => $row['kurs'],
                'Iznos kursa' => $row['iznos_kursa'],
                'Konvertovana vrednost' => $row['konv_iznos']
        ];
    } 
    // We want the final result to ignore the keys and to create a JSON array not a JSON object 
   /*$data = [];
    foreach ($json_response as $element) {
        $data[] = $element;
    }*/
}
$json = fopen('convlist.json', 'w');
fwrite($json, json_encode($json_response,JSON_PRETTY_PRINT));
fclose($json);

//print '<pre>'.json_encode($json_response, JSON_PRETTY_PRINT).'</pre>';
//print '<pre>'.json_encode($data, JSON_PRETTY_PRINT).'</pre>';
//echo json_encode($data, JSON_PRETTY_PRINT);
